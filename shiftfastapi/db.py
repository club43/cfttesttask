import sqlalchemy
import databases
import ormar
import datetime

from .config import settings

database = databases.Database(settings.db_url)
metadata = sqlalchemy.MetaData()


class BaseMeta(ormar.ModelMeta):
    metadata = metadata
    database = database

# Модель таблицы с зарплатой пользователя


class Salary(ormar.Model):
    class Meta(BaseMeta):
        tablename = 'salary'
    token: str = ormar.String(primary_key=True, max_length=128, nullable=True)
    value: int = ormar.Integer(default=10000, nullable=True)
    date: str = ormar.String(default=str(
        datetime.date(2023, 12, 26)), max_length=12)
    timestamp: str = ormar.String(nullable=True, max_length=12)
    username: str = ormar.String(nullable=True, max_length=128)

# Модель таблицы пользователей


class Employee(ormar.Model):
    class Meta(BaseMeta):
        tablename = 'employee'

    id: int = ormar.Integer(primary_key=True)
    username: str = ormar.String(max_length=128, unique=True, nullable=False)
    password: str = ormar.String(max_length=128, nullable=False)
    tokenTimestamp: str = ormar.String(max_length=128, nullable=False)


engine = sqlalchemy.create_engine(settings.db_url)
metadata.create_all(engine)
