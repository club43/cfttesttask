from .db import database, Employee, Salary
import hashlib
from .config import settings
import datetime
from random_word import RandomWords

token_salt = settings.token_salt
password_salt = settings.password_salt

# Вспомогательные функции


def stringToDt(string):
    return datetime.datetime.strptime(string, '%Y-%m-%d').date()


def generateHash(string):
    return hashlib.md5(string.encode()).hexdigest()


async def dbConnect():
    if not database.is_connected:
        await database.connect()


async def getToken(username, password):

    hashed_password = generateHash(password)
    res = await Employee.objects.all()
    exists = False

    for users in res:
        if users.username == username:
            exists = True
            break

    if not exists:

        timestamp = str(datetime.datetime.now().date())
        await Employee.objects.create(username=username, password=hashed_password, tokenTimestamp=timestamp)

        r = RandomWords()
        token = generateHash(r.get_random_word())

        await Salary.objects.create(token=token, timestamp=timestamp, username=username)

        return {'token': token}

    else:
        user = await Employee.objects.get(username=username)

        if user.password != hashed_password:

            return {'response': "wrong password"}

        elif user.password == hashed_password:
            res = await Employee.objects.get(username=username)
            tokenTimestamp = res.tokenTimestamp
            timestamp = datetime.datetime.now().date()
            tokenTimestamp = stringToDt(tokenTimestamp)

            r = RandomWords()
            newToken = generateHash(r.get_random_word())

            await Salary.objects.filter(username=username).update(token=newToken, timestamp=str(timestamp))
            await Salary.objects.filter(username=username).update(token=newToken, timestamp=str(datetime.datetime.now().date()))

            return {'restored token': newToken}


async def getInfo(token, username):
    res = await Salary.objects.get(token=token, username=username)
    value = res.value
    promotion = res.date
    return {
        "User": username,
        "Salary value": value,
        "Promotion date": promotion
    }


async def updateTokens():
    res = await Salary.objects.all()
    r = RandomWords()
    for items in res:
        if stringToDt(items.timestamp) + datetime.timedelta(days=1) < datetime.datetime.now().date():
            tempWrd = r.get_random_word()
            oldToken = items.token
            newToken = generateHash(tempWrd)
            await Salary.objects.filter(token=oldToken).update(token=newToken, timestamp=str(datetime.datetime.now().date()))
            await Employee.objects.update(tokenTimestamp=str(datetime.datetime.now().date()))
