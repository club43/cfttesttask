import uvicorn
from fastapi import FastAPI, Body
from .db import database, Employee, Salary
from .queries import getToken, updateTokens, getInfo

app = FastAPI()


@app.on_event('startup')
async def startup():
    if not database.is_connected:
        await database.connect()
        await updateTokens()


@app.on_event("shutdown")
async def shutdown():
    if database.is_connected:
        await updateTokens()
        await database.disconnect()


@app.post('/auth')
async def auth(username: str = Body(...),
               password: str = Body(...)):
    return await getToken(username, password)


@app.get('/info')
async def info(token: str = Body(...),
               username: str = Body(...)):
    return await getInfo(token, username)

# Маршруты для тестирования


@app.get('/getUsers')
async def read():
    return await Employee.objects.all()


@app.get('/getTokens')
async def readtokens():
    return await Salary.objects.all()


def main():
    uvicorn.run(app, host='0.0.0.0', port=8000)


if __name__ == '__main__':
    main()
