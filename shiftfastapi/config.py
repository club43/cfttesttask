import os

from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    db_url: str = Field(..., env='DATABASE_URL')
    password_salt: str = Field(..., env='PASSWORD_SALT')
    token_salt: str = Field(..., env='TOKEN_SALT')

settings = Settings()