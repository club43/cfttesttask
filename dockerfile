FROM python:3.11.0

WORKDIR /shiftfastapi
COPY poetry.lock .
COPY pyproject.toml .
RUN pip install poetry
RUN poetry install
COPY . .
